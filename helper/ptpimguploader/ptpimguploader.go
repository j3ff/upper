package ptpimguploader

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
)

type ImageInfo struct {
	Code string `json:"code"`
	Ext  string `json:"ext"`
}

func UploadFiles(folderPath string, apiKey string) ([]string, error) {
	url := "https://ptpimg.me/upload.php"
	var requestBody bytes.Buffer
	multipartWriter := multipart.NewWriter(&requestBody)

	_ = multipartWriter.WriteField("api_key", apiKey)

	files, err := os.ReadDir(folderPath)
	if err != nil {
		return nil, err
	}

	for _, file := range files {
		if !file.IsDir() {
			filePath := filepath.Join(folderPath, file.Name())
			fileWriter, fwerr := multipartWriter.CreateFormFile("file-upload[]", file.Name())
			if fwerr != nil {
				return nil, fwerr
			}
			f, fwerr := os.Open(filePath)
			if fwerr != nil {
				return nil, fwerr
			}
			defer f.Close()
			_, fwerr = io.Copy(fileWriter, f)
			if fwerr != nil {
				return nil, fwerr
			}
		}
	}

	multipartWriter.Close()

	request, err := http.NewRequest("POST", url, &requestBody)
	if err != nil {
		return nil, err
	}
	request.Header.Set("Content-Type", multipartWriter.FormDataContentType())

	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		return nil, errors.New("upload failed with status code: " + fmt.Sprint(response.StatusCode))
	}

	var images []ImageInfo
	err = json.NewDecoder(response.Body).Decode(&images)
	if err != nil {
		return nil, err
	}

	var urls []string
	for _, img := range images {
		urls = append(urls, "[img]https://ptpimg.me/"+img.Code+"."+img.Ext+"[/img]")
	}

	if len(urls) > 0 {
		fileInfo, err := os.Stdout.Stat()
		if err != nil {
			fmt.Println("Error:", err)
			return nil, err
		}
		if (fileInfo.Mode() & os.ModeCharDevice) != 0 {
			fmt.Println("Images Uploaded Successfully")
		}
	}

	return urls, nil
}
