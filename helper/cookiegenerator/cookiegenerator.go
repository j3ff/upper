package cookiegenerator

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"strings"

	"golang.org/x/net/html"
)

type CookieInfo struct {
	CookieName  string `json:"cookie_name"`
	CookieValue string `json:"cookie_value"`
}

func GenerateCookies(username string, password string) ([]byte, error) {
	urlStr := "https://karagarga.in/takelogin.php"
	targetURL := "https://iup.crowing.me"
	authURL := "https://iup.crowing.me/auth"

	jar, err := cookiejar.New(nil)
	if err != nil {
		return nil, err
	}

	client := &http.Client{
		Jar: jar,
	}

	data := url.Values{
		"username": {username},
		"password": {password},
	}

	body := bytes.NewBufferString(data.Encode())

	req, err := http.NewRequest("POST", urlStr, body)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	_, err = io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	resp, err = client.Get(targetURL)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	doc, err := html.Parse(strings.NewReader(string(respBody)))
	if err != nil {
		return nil, err
	}

	key := extractKey(doc, "key")

	data = url.Values{"key": {key}}
	body = bytes.NewBufferString(data.Encode())

	req, err = http.NewRequest("POST", authURL, body)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err = client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	cookies := []CookieInfo{
		{
			CookieName:  "access-token",
			CookieValue: key,
		},
	}
	jsonCookies, err := json.Marshal(cookies)
	if err != nil {
		return nil, err
	}

	return jsonCookies, nil
}


func extractKey(n *html.Node, name string) string {
	if n.Type == html.ElementNode && n.Data == "input" {
		for _, attr := range n.Attr {
			if attr.Key == "name" && attr.Val == name {
				for _, a := range n.Attr {
					if a.Key == "value" {
						return a.Val
					}
				}
			}
		}
	}

	for c := n.FirstChild; c != nil; c = c.NextSibling {
		if val := extractKey(c, name); val != "" {
			return val
		}
	}

	return ""
}
