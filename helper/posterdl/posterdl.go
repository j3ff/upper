package posterdl

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/antchfx/htmlquery"
	tmdb "github.com/cyruzin/golang-tmdb"
)

func GetTmdbID(url string) (string, error) {
	basePath := strings.TrimSuffix(url, "/")
	basePathArr := strings.Split(basePath, "/")

	re := regexp.MustCompile(`\d+`)
	matches := re.FindAllString(basePathArr[len(basePathArr)-1], -1)

	if len(matches) > 0 {
		return matches[0], nil
	}

	return "", fmt.Errorf("unable to extract movie ID from URL")
}

func TmdbApidl(url, apiKey string) (imgURL string, err error) {
	var tmdbIDInt int
	if strings.Contains(url, "/movie/") {
		tmdbID, err := GetTmdbID(url)
		if err != nil {
			return "", err
		}
		tmdbIDInt, err = strconv.Atoi(tmdbID)
		if err != nil {
			return "", err
		}

		tmdbClient, err := tmdb.Init(apiKey)
		if err != nil {
			return "", err
		}

		movie, err := tmdbClient.GetMovieDetails(tmdbIDInt, nil)
		if err != nil {
			return "", err
		}

		imgURL = tmdb.GetImageURL(movie.PosterPath, tmdb.Original)

	} else if strings.Contains(url, "/tv/") {
		tmdbID, err := GetTmdbID(url)
		if err != nil {
			return "", err
		}
		tmdbIDInt, err = strconv.Atoi(tmdbID)
		if err != nil {
			return "", err
		}

		tmdbClient, err := tmdb.Init(apiKey)
		if err != nil {
			return "", err
		}

		tv, err := tmdbClient.GetTVDetails(tmdbIDInt, nil)
		if err != nil {
			return "", err
		}

		imgURL = tmdb.GetImageURL(tv.PosterPath, tmdb.Original)

	} else {
		return "", errors.New("unsupported URL format")
	}
	return imgURL, nil
}

func Tmdbimg(url string) (string, error) {
	xpathExpr := "//div[contains(@class, 'poster_wrapper')]//div[contains(@class, 'poster')]//div[contains(@class, 'image_content')]//img[contains(@class, 'poster')]/@src"

	client := &http.Client{}

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}

	req.Header.Set(
		"User-Agent",
		"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36",
	)

	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	htmlContent := string(body)

	doc, err := htmlquery.Parse(strings.NewReader(htmlContent))
	if err != nil {
		return "", err
	}

	node := htmlquery.FindOne(doc, xpathExpr)
	if node == nil {
		return "", fmt.Errorf("poster image not found")
	}

	value := htmlquery.SelectAttr(node, "src")

	lastIndex := strings.LastIndex(value, "/")
	if lastIndex == -1 || lastIndex == len(value)-1 {
		return "", fmt.Errorf("invalid poster image URL")
	}
	filename := value[lastIndex+1:]

	imageURL := "https://image.tmdb.org/t/p/original/" + filename
	return imageURL, nil
}

func Imdbimg(url string) (string, error) {
	client := &http.Client{
		Timeout: 0,
	}

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", fmt.Errorf("error creating request of IMDB: %v", err)
	}

	req.Header.Set(
		"User-Agent",
		"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36",
	)

	resp, err := client.Do(req)
	if err != nil {
		return "", fmt.Errorf("error making request: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("unexpected status code: %d", resp.StatusCode)
	}

	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return "", fmt.Errorf("error parsing HTML: %v", err)
	}

	var jsonStr string
	doc.Find("#__NEXT_DATA__").Each(func(i int, s *goquery.Selection) {
		jsonStr = s.Text()
	})

	if jsonStr == "" {
		return "", fmt.Errorf("error fetching imbdb props")
	}

	var data map[string]interface{}
	if err := json.Unmarshal([]byte(jsonStr), &data); err != nil {
		return "", fmt.Errorf("error parsing imdb props: %v", err)
	}

	props, ok := data["props"].(map[string]interface{})
	if !ok || props == nil {
		return "", fmt.Errorf("poster image URL not found")
	}

	pageProps, ok := props["pageProps"].(map[string]interface{})
	if !ok || pageProps == nil {
		return "", fmt.Errorf("poster image URL not found")
	}

	aboveTheFoldData, ok := pageProps["aboveTheFoldData"].(map[string]interface{})
	if !ok || aboveTheFoldData == nil {
		return "", fmt.Errorf("poster image URL not found")
	}

	primaryImage, ok := aboveTheFoldData["primaryImage"].(map[string]interface{})
	if !ok || primaryImage == nil {
		return "", fmt.Errorf("poster image URL not found")
	}

	imageURL, ok := primaryImage["url"].(string)
	if !ok {
		return "", fmt.Errorf("poster image URL not found")
	}

	return imageURL, nil
}

func Omdbimg(url string, omdbKey string) (string, error) {
	imdbIDRegex := regexp.MustCompile(`tt\d+`)
	imdbIDMatches := imdbIDRegex.FindStringSubmatch(url)
	if len(imdbIDMatches) < 1 {
		return "", fmt.Errorf("imdb ID not found in URL")
	}
	imdbID := imdbIDMatches[0]

	omdbURL := fmt.Sprintf("http://www.omdbapi.com/?i=%s&apikey=%s", imdbID, omdbKey)

	client := &http.Client{
		Timeout: time.Second * 5,
	}

	resp, err := client.Get(omdbURL)
	if err != nil {
		return "", fmt.Errorf("error making request to OMDB API: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("unexpected status code of OMDB request: %d", resp.StatusCode)
	}

	var omdbResponse map[string]interface{}
	err = json.NewDecoder(resp.Body).Decode(&omdbResponse)
	if err != nil {
		return "", fmt.Errorf("error parsing OMDB API response: %v", err)
	}

	if omdbResponse == nil {
		return "", fmt.Errorf("empty response from OMDB API")
	}

	posterURL, ok := omdbResponse["Poster"].(string)
	if !ok || posterURL == "N/A" {
		return "", fmt.Errorf("poster URL not found or N/A in OMDB API response")
	}

	lastUnderscoreIndex := strings.LastIndex(posterURL, "_")
	if lastUnderscoreIndex != -1 {
		posterURL = posterURL[:lastUnderscoreIndex] + ".jpg"
	}

	return posterURL, nil
}

func DownloadImage(url string, folderPath string) error {
	if err := os.MkdirAll(folderPath, 0755); err != nil {
		return err
	}

	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			req.Header.Set(
				"User-Agent",
				"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36",
			)
			return nil
		},
	}

	resp, err := client.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	fileName := "poster.jpg"
	if disposition := resp.Header.Get("Content-Disposition"); disposition != "" {
		if strings.Contains(disposition, "filename=") {
			parts := strings.Split(disposition, "filename=")
			fileName = strings.Trim(parts[1], "\"")
		}
	}

	filePath := filepath.Join(folderPath, fileName)
	file, err := os.Create(filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = io.Copy(file, resp.Body)
	if err != nil {
		return err
	}

	// fmt.Printf("Poster downloaded successfully: %s\n", filePath)
	return nil
}

func Dl(urlString, folderPath string) error {
	parsedURL, err := url.Parse(urlString)
	if err != nil {
		return err
	}

	hostname := strings.ToLower(parsedURL.Hostname())
	if strings.Contains(hostname, "themoviedb") {
		// fmt.Println("Tmdb detected")
		imgURL, err := Tmdbimg(urlString)
		if err != nil {
			return err
		}
		if err := DownloadImage(imgURL, folderPath); err != nil {
			return err
		}
	} else if strings.Contains(hostname, "imdb") {
		// fmt.Println("Imdb detected")
		imgURL, err := Imdbimg(urlString)
		if err != nil {
			return err
		}
		if err := DownloadImage(imgURL, folderPath); err != nil {
			return err
		}
	} else {
		return fmt.Errorf("unsupported URL: only TMDB or IMDB URLs are supported")
	}

	return nil
}

func DlAPI(urlString, omdbAPI, tmdbAPI, folderPath string) error {
	if omdbAPI == "" || tmdbAPI == "" {
		return nil
	}

	var imgURL string
	var err error

	if strings.Contains(urlString, ".imdb") {
		imgURL, err = Omdbimg(urlString, omdbAPI)
	} else if strings.Contains(urlString, "themoviedb") {
		imgURL, err = TmdbApidl(urlString, tmdbAPI)
	} else {
		return errors.New("unsupported URL: only TMDB or IMDB URLs are supported")
	}

	if err != nil {
		return err
	}

	if imgURL != "" {
		if err := DownloadImage(imgURL, folderPath); err != nil {
			return err
		}
	}

	return nil
}
