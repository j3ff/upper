package usageinteractor

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"upper/helper/config"
	"upper/helper/cookiefilechecker"
	"upper/helper/imguploader"
	"upper/helper/posterdl"
	"upper/helper/ptpimguploader"
	"upper/helper/torrentcreator"
)

func getDefaultEnvFile() string {
	var defaultEnvFile string
	if runtime.GOOS == "windows" {
		homeDir, _ := os.UserHomeDir()
		defaultEnvFile = filepath.Join(homeDir, "Documents", "upper_env.json")
	} else {
		homeDir, _ := os.UserHomeDir()
		defaultEnvFile = filepath.Join(homeDir, ".config", "upper_env.json")
	}
	return defaultEnvFile
}

func Run() {
	reader := bufio.NewReader(os.Stdin)

	fmt.Println("Select an option:")
	fmt.Println("1. Upload Screenshots")
	fmt.Println("2. Create Torrent")
	fmt.Println("3. Both")
	fmt.Println("4. Quit")

	fmt.Print("Enter number: ")
	optionStr, _ := reader.ReadString('\n')
	option := strings.TrimSpace(optionStr)

	switch option {
	case "1":
		handleScreenshotsUpload(reader)
	case "2":
		handleTorrentCreation(reader)
	case "3":
		handleScreenshotsUpload(reader)
		handleTorrentCreation(reader)
	case "4":
		os.Exit(0)
	default:
		fmt.Println("Invalid option")
	}
}

func handleScreenshotsUpload(reader *bufio.Reader) {
	var ssFolderPath string

	for {
		fmt.Print("Enter screenshot folder path: ")
		ssFolderPath, _ = reader.ReadString('\n')
		ssFolderPath = strings.TrimSpace(ssFolderPath)
		var err error
		ssFolderPath, err = ExpandTilde(ssFolderPath)
		if err != nil {
			fmt.Println("Error expanding tilde:", err)
			continue
		}

		if ssFolderPath != "" {
			if _, err := os.Stat(ssFolderPath); os.IsNotExist(err) {
				fmt.Println("The specified folder path does not exist. Please enter a valid path.")
				continue
			}
			break
		}
		fmt.Println("Screenshot folder path cannot be empty. Please try again.")
	}

	var includePoster string
	var posterURL string

	for {
		fmt.Print("Include poster? (y/n): ")
		includePoster, _ = reader.ReadString('\n')
		includePoster = strings.TrimSpace(strings.ToLower(includePoster))

		if includePoster == "yes" || includePoster == "y" {
			fmt.Print("Enter poster URL: ")
			posterURL, _ = reader.ReadString('\n')
			posterURL = strings.TrimSpace(posterURL)
			handlePosterURL(posterURL, &ssFolderPath)
			break
		} else if includePoster == "no" || includePoster == "n" {
			break
		}
		fmt.Println("Invalid input. Please enter yes or no.")
	}

	mode := "i"

	fmt.Printf("Default mode is IPFS (i)\n")
	fmt.Print("Do you want to change the mode? (y/n): ")
	changeMode, _ := reader.ReadString('\n')
	changeMode = strings.TrimSpace(strings.ToLower(changeMode))

	if changeMode == "yes" || changeMode == "y" {
		fmt.Println("Select mode:")
		fmt.Println("1. IPFS (i)")
		fmt.Println("2. PTPImg (p)")

		for {
			fmt.Print("Enter number: ")
			modeStr, _ := reader.ReadString('\n')
			modeStr = strings.TrimSpace(modeStr)

			if modeStr == "1" || strings.ToLower(modeStr) == "i" {
				mode = "i"
				break
			} else if modeStr == "2" || strings.ToLower(modeStr) == "p" {
				mode = "p"
				break
			}
			fmt.Println("Invalid input. Please enter '1' or 'i' for IPFS, '2' or 'p' for PTPImg.")
		}
	}
	switch mode {
	case "i":
		handleModeI(ssFolderPath)
	case "p":
		handleModeP(ssFolderPath)
	default:
		fmt.Println("Invalid mode")
	}
}

func ExpandTilde(path string) (string, error) {
	if strings.HasPrefix(path, "~/") {
		homeDir, err := os.UserHomeDir()
		if err != nil {
			return "", err
		}
		return strings.Replace(path, "~", homeDir, 1), nil
	}
	return path, nil
}

func handleTorrentCreation(reader *bufio.Reader) {
	for {
		fmt.Print("Enter video file or folder path: ")
		videoPath, _ := reader.ReadString('\n')
		videoPath = strings.TrimSpace(videoPath)
		var err error
		videoPath, err = ExpandTilde(videoPath)
		if err != nil {
			fmt.Println("Error expanding tilde:", err)
			continue
		}

		if videoPath != "" {
			if _, err := os.Stat(videoPath); os.IsNotExist(err) {
				fmt.Println("The entered path does not exist. Please enter a correct path.")
				continue
			}
			handleCreateTorrent(true, videoPath)
			break
		} else {
			fmt.Println("Please enter a non-empty path.")
		}
	}
}

func handleDefaultEnvFile(defaultEnvFile string) string {
	if _, err := os.Stat(defaultEnvFile); os.IsNotExist(err) {
		fmt.Println("Default environment file not found:", defaultEnvFile)
		fmt.Println("Please specify the path to upper_env.json using the -env flag.")
		os.Exit(1)
	}
	return defaultEnvFile
}

func handlePosterURL(posterURL string, ssfolderpath *string) {
	if posterURL != "" {
		if *ssfolderpath == "" {
			currentDir, cerr := os.Getwd()
			if cerr != nil {
				fmt.Println(cerr)
				os.Exit(1)
			}
			*ssfolderpath = filepath.Join(currentDir, "upperss")
			cerr = os.MkdirAll(*ssfolderpath, 0755)
			if cerr != nil {
				fmt.Println("Error creating upperss folder:", cerr)
				os.Exit(1)
			}
		}

		var poserr error
		poserr = posterdl.Dl(posterURL, *ssfolderpath)
		if poserr != nil {
			defaultEnvFile := getDefaultEnvFile()
			config, err := config.LoadEnvConfig(defaultEnvFile)
			if err != nil {
				fmt.Println("Error loading config:", err)
				os.Exit(1)
			}
			if config.OmdbApikey != "" || config.TmdbApikey != "" {
				omdbAPI := config.OmdbApikey
				tmdbAPI := config.TmdbApikey
				poserr = posterdl.DlAPI(posterURL, omdbAPI, tmdbAPI, *ssfolderpath)
			}
			if poserr != nil {
				fmt.Println("Error downloading poster:", poserr)
			}
		}
	}
}

func handleModeI(ssfolderpath string) {
	defaultEnvFile := getDefaultEnvFile()
	config, err := config.LoadEnvConfig(defaultEnvFile)
	if err != nil {
		fmt.Println("Error loading config:", err)
		os.Exit(1)
	}

	if ssfolderpath != "" {
		cookieFilePath := config.CookiePath
		if cookieFilePath == "" {
			fmt.Println("Error: Please specify cookie file path in upper_env.json")
			os.Exit(1)
		}
		handleCookieFile(cookieFilePath)
		url := "wss://iup.crowing.me/socket.io/?EIO=4&transport=websocket"
		folderPath := ssfolderpath
		cookiesPath := cookieFilePath
		uploader := imguploader.NewImageUploader(url, folderPath, cookiesPath)
		uploader.Run()
	} else {
		fmt.Println()
	}
}

func handleCookieFile(cookieFilePath string) {
	defaultEnvFile := getDefaultEnvFile()
	config, err := config.LoadEnvConfig(defaultEnvFile)
	if err != nil {
		fmt.Println("Error loading config:", err)
		os.Exit(1)
	}

	_, ckerr := os.Stat(cookieFilePath)
	if ckerr != nil {
		if os.IsNotExist(ckerr) {
			_, ferr := os.Create(cookieFilePath)
			if ferr != nil {
				fmt.Println("Error:", ferr)
				os.Exit(1)
			}
		} else {
			fmt.Println("Error:", ckerr)
			os.Exit(1)
		}
	}
	cksData, ckerr := os.ReadFile(cookieFilePath)
	if ckerr != nil {
		log.Fatal("Error reading cookie file:", ckerr)
	}
	var cks []struct {
		CkName  string `json:"cookie_name"`
		CkValue string `json:"cookie_value"`
	}
	ckerr = json.Unmarshal(cksData, &cks)
	if ckerr != nil || len(cks) == 0 || cks[0].CkValue == "" {
		ckerr = os.Remove(cookieFilePath)
		if ckerr != nil {
			log.Fatal("Error deleting cookie file:", ckerr)
		}
		if config.Username == "" || config.Password == "" {
			fmt.Println("Error: Ensure to have both username and password in upper_env.json")
			os.Exit(1)
		}
		cookiefilechecker.Cookiescheckergen(config.Username, config.Password, cookieFilePath)
	}
}

func handleModeP(ssfolderpath string) {
	defaultEnvFile := getDefaultEnvFile()
	config, err := config.LoadEnvConfig(defaultEnvFile)
	if err != nil {
		fmt.Println("Error loading config:", err)
		os.Exit(1)
	}

	if ssfolderpath != "" {
		apiKey := config.PtpImgApikey
		urls, perr := ptpimguploader.UploadFiles(ssfolderpath, apiKey)
		if perr != nil {
			log.Fatal("Error:", perr)
		}
		for _, url := range urls {
			fmt.Println(url)
		}
	} else {
		fmt.Println()
	}
}

func handleCreateTorrent(ct bool, videoPath string) {
	defaultEnvFile := getDefaultEnvFile()
	config, err := config.LoadEnvConfig(defaultEnvFile)
	if err != nil {
		fmt.Println("Error loading config:", err)
		os.Exit(1)
	}

	if ct && videoPath == "" {
		fmt.Println("Please provide file/folder path to create torrent using -f.")
		flag.Usage()
		os.Exit(1)
	}

	if ct && config.AnnounceUrl != "" && config.TorrentSavePath != "" {
		torrentSavePath := config.TorrentSavePath
		announceURL := config.AnnounceUrl
		workers := config.Workers
		torerr := torrentcreator.CreateTorrent(videoPath, announceURL, torrentSavePath, workers)
		if torerr != nil {
			log.Fatal("Error creating torrent file:", torerr)
		}
	} else if ct {
		log.Fatal("Please provide both AnnounceURL and TorrentSavePath in the configuration")
	}
}
