package imguploader

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/gorilla/websocket"
)

type Cookies struct {
	CookieName  string `json:"cookie_name"`
	CookieValue string `json:"cookie_value"`
}

type ImageUploader struct {
	url         string
	folderPath  string
	sid         string
	cookiesPath string
}

func NewImageUploader(url, folderPath, cookiesPath string) *ImageUploader {
	return &ImageUploader{
		url:         url,
		folderPath:  folderPath,
		cookiesPath: cookiesPath,
	}
}

func formatIPFSUrls(message []byte) ([]string, error) {
	var data []interface{}
	err := json.Unmarshal(message[2:], &data)
	if err != nil {
		return nil, fmt.Errorf("error decoding JSON: %w", err)
	}

	fileList, ok := data[1].(map[string]interface{})["files"].([]interface{})
	if !ok {
		return nil, fmt.Errorf("files not found in JSON data")
	}
	folderHash, ok := data[1].(map[string]interface{})["folderHash"].(string)
	if !ok {
		return nil, fmt.Errorf("folderHash not found in JSON data")
	}

	urls := make([]string, len(fileList))
	for i, f := range fileList {
		fileName, ok := f.(string)
		if !ok {
			return nil, fmt.Errorf("invalid file name in JSON data")
		}
		urls[i] = fmt.Sprintf("[img]https://iup.crowing.me/ipfs/%s/%s[/img]", folderHash, fileName)
	}

	return urls, nil
}

func (iu *ImageUploader) onMessage(ws *websocket.Conn, message string) bool {
	// fmt.Println("Received message:", message)
	if strings.HasPrefix(message, "40") {
		iu.sid = strings.Split(strings.Split(message, `"sid":"`)[1], `"`)[0]
		if iu.sid != "" {
			iu.uploadImage()
		}
	} else if message == "2" {
		// fmt.Println("Sent message:", "3")
		ws.WriteMessage(websocket.TextMessage, []byte("3"))
	} else if strings.Contains(message, "42") {
		if strings.Contains(message, "upload-status") && strings.Contains(message, "success") {
			messageBytes := []byte(message)

			urls, err := formatIPFSUrls(messageBytes)
			if err != nil {
				fmt.Println("Error:", err)
				return false
			}

			for _, url := range urls {
				fmt.Println(url)
			}
			defer ws.Close()
			return true
		}
	}
	return false
}

func (iu *ImageUploader) Run() {
	cookiesData, err := os.ReadFile(iu.cookiesPath)
	if err != nil {
		fmt.Println("Error reading cookies file:", err)
		return
	}

	var cookiesInfo []Cookies
	err = json.Unmarshal(cookiesData, &cookiesInfo)
	if err != nil {
		fmt.Println("Error decoding cookies JSON:", err)
		return
	}

	var accessToken string
	for _, cookie := range cookiesInfo {
		if cookie.CookieName == "access-token" {
			accessToken = cookie.CookieValue
			break
		}
	}

	header := http.Header{}

	if accessToken != "" {
		header.Set("Cookie", fmt.Sprintf("access-token=%s", accessToken))
	}

	ws, _, err := websocket.DefaultDialer.Dial(iu.url, header)
	if err != nil {
		fmt.Println("Error connecting to websocket:", err)
		return
	}
	defer ws.Close()

	var formatIPFSUrlsCalled bool

	done := make(chan struct{})

	go func() {
		for {
			_, message, err := ws.ReadMessage()
			if err != nil {
				iu.onClosed(ws)
				break
			}
			formatIPFSUrlsCalled = iu.onMessage(ws, string(message))
			if formatIPFSUrlsCalled {
				close(done)
				break
			}
		}
	}()

	iu.onOpen(ws)

	<-done
}

func (iu *ImageUploader) onOpen(ws *websocket.Conn) {
	time.Sleep(1 * time.Second)
	// message := "40"
	// fmt.Println("Sending message:", message)
	ws.WriteMessage(websocket.TextMessage, []byte("40"))
}

func (iu *ImageUploader) onClosed(ws *websocket.Conn) {
	// fmt.Println("WebSocket connection closed")
}

func (iu *ImageUploader) uploadImage() {
	if iu.sid != "" {
		cookiesData, err := os.ReadFile(iu.cookiesPath)
		if err != nil {
			fmt.Println("Error reading cookies file:", err)
			return
		}
		var cookies []Cookies
		err = json.Unmarshal(cookiesData, &cookies)
		if err != nil {
			fmt.Println("Error decoding cookies JSON:", err)
			return
		}

		files := []*os.File{}
		filepath.Walk(iu.folderPath, func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if !info.IsDir() &&
				(strings.HasSuffix(strings.ToLower(info.Name()), ".png") || strings.HasSuffix(strings.ToLower(info.Name()), ".jpg") || strings.HasSuffix(strings.ToLower(info.Name()), ".jpeg") || strings.HasSuffix(strings.ToLower(info.Name()), ".gif")) {
				file, err := os.Open(path)
				if err != nil {
					return err
				}
				files = append(files, file)
			}
			return nil
		})

		body := &bytes.Buffer{}
		writer := multipart.NewWriter(body)
		for _, file := range files {
			part, ierr := writer.CreateFormFile("image", filepath.Base(file.Name()))
			if ierr != nil {
				fmt.Println("Error creating form file:", ierr)
				return
			}
			_, ierr = io.Copy(part, file)
			if ierr != nil {
				fmt.Println("Error copying file data:", ierr)
				return
			}
		}
		writer.WriteField("sid", iu.sid)
		writer.Close()

		req, err := http.NewRequest("POST", "https://iup.crowing.me/upload", body)
		if err != nil {
			fmt.Println("Error creating request:", err)
			return
		}
		req.Header.Set("Content-Type", writer.FormDataContentType())

		for _, cookie := range cookies {
			req.AddCookie(&http.Cookie{Name: cookie.CookieName, Value: cookie.CookieValue})
		}

		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			fmt.Println("Error sending request:", err)
			return
		}
		defer resp.Body.Close()
		if resp.StatusCode == 200 {
			fileInfo, err := os.Stdout.Stat()
			if err != nil {
				fmt.Println("Error:", err)
				return
			}
			if (fileInfo.Mode() & os.ModeCharDevice) != 0 {
				fmt.Println("Images Uploaded Successfully")
			}
		} else {
			fmt.Println("Failed to upload images")
			os.Exit(1)
		}
	}
}
