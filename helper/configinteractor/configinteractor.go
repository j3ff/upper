package configinteractor

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"

	"golang.org/x/term"
)

var homeDir string

func init() {
	homeDir, _ = os.UserHomeDir()
}

type EnvConfig struct {
	PtpImgApikey    string `json:"PTPImgAPI"`
	Username        string `json:"Username"`
	Password        string `json:"Password"`
	OmdbApikey      string `json:"OmdbApi"`
	TmdbApikey      string `json:"TmdbApi"`
	CookiePath      string `json:"CookiePath"`
	AnnounceUrl     string `json:"AnnounceURL"`
	Workers         int    `json:"workers"`
	TorrentSavePath string `json:"TorrentSavePath"`
}

func ConfigInteractor() {
	config := EnvConfig{}

	defaultPath := defaultConfigPath()
	if _, err := os.Stat(defaultPath); err == nil {
		existingConfig, err := loadConfigFromFile(defaultPath)
		if err != nil {
			fmt.Println("Error loading existing config:", err)
		} else {
			config = existingConfig
		}
	}

	if config.TorrentSavePath == "" {
		config.TorrentSavePath = defaultTorrentSavePath()
	}
	if config.CookiePath == "" {
		config.CookiePath = defaultCookiePath()
	}

	config.Workers = 3

	ptpImgAPIKey := askForInput("PTPImgAPI (default: " + config.PtpImgApikey + "): ")
	if ptpImgAPIKey != "" {
		config.PtpImgApikey = ptpImgAPIKey
	}

	username := askForInput("Username (default: " + config.Username + "): ")
	if username != "" {
		config.Username = username
	}

	password := askForInput("Password (default: " + config.Password + "): ")
	if password != "" {
		config.Password = password
	}

	omdbAPIKey := askForInput("OmdbApi (default: " + config.OmdbApikey + "): ")
	if omdbAPIKey != "" {
		config.OmdbApikey = omdbAPIKey
	}

	tmdbAPIKey := askForInput("TmdbApi (default: " + config.TmdbApikey + "): ")
	if tmdbAPIKey != "" {
		config.TmdbApikey = tmdbAPIKey
	}

	cookiePath := askForInput("CookiePath (default: " + config.CookiePath + "): ")
	if cookiePath != "" {
		config.CookiePath = cookiePath
	}

	torrentSavePath := askForInput("TorrentSavePath (default: " + config.TorrentSavePath + "): ")
	if torrentSavePath != "" {
		config.TorrentSavePath = torrentSavePath
	}

	announceURL := askForInput("AnnounceUrl (default: " + config.AnnounceUrl + "): ")
	if announceURL != "" {
		config.AnnounceUrl = announceURL
	}

	workers := askForInput(fmt.Sprintf("Workers (default: %d): ", config.Workers))
	if workers != "" {
		config.Workers = parseInt(workers)
	}

	saveConfigToFile(config)
}

func defaultConfigPath() string {
	var defaultPath string
	if runtime.GOOS == "windows" {
		defaultPath = filepath.Join(homeDir, "Documents", "upper_env.json")
	} else {
		defaultPath = filepath.Join(homeDir, ".config", "upper_env.json")
	}
	return defaultPath
}

func loadConfigFromFile(filePath string) (EnvConfig, error) {
	var config EnvConfig

	file, err := os.Open(filePath)
	if err != nil {
		return config, err
	}
	defer file.Close()

	decoder := json.NewDecoder(file)
	err = decoder.Decode(&config)
	if err != nil {
		return config, err
	}

	return config, nil
}

func defaultCookiePath() string {
	var defaultPath string
	if runtime.GOOS == "windows" {
		defaultPath = filepath.Join(homeDir, "Documents", "upper_cookies.json")
	} else {
		defaultPath = filepath.Join(homeDir, ".config", "upper_cookies.json")
	}
	return defaultPath
}

func defaultTorrentSavePath() string {
	var path string
	if runtime.GOOS == "windows" {
		path = filepath.Join(homeDir, "Documents", "upper_torrentfiles")
	} else {
		path = filepath.Join(homeDir, ".config", "upper_torrentfiles")
	}

	if _, err := os.Stat(path); os.IsNotExist(err) {
		err := os.MkdirAll(path, 0755)
		if err != nil {
			fmt.Println("Error creating directory:", err)
		}
	}

	return path
}

func askForInput(prompt string) string {
	fmt.Println()
	fmt.Print(prompt)
	if strings.Contains(prompt, "Password") || strings.Contains(prompt, "AnnounceUrl") {
		input, err := term.ReadPassword(int(os.Stdin.Fd()))
		if err != nil {
			fmt.Println("Error reading input:", err)
			return ""
		}
		fmt.Println()
		return strings.TrimSpace(string(input))
	}
	reader := bufio.NewReader(os.Stdin)
	input, _ := reader.ReadString('\n')
	return strings.TrimSpace(input)
}

func saveConfigToFile(config EnvConfig) {
	var defaultPath string
	if runtime.GOOS == "windows" {
		defaultPath = filepath.Join(homeDir, "Documents", "upper_env.json")
	} else {
		defaultPath = filepath.Join(homeDir, ".config", "upper_env.json")
	}

	dir := filepath.Dir(defaultPath)
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err := os.MkdirAll(dir, 0755)
		if err != nil {
			fmt.Println("Error creating directory:", err)
			return
		}
	}

	encodedConfig, err := json.MarshalIndent(config, "", "    ")
	if err != nil {
		fmt.Println("Error encoding JSON:", err)
		return
	}

	file, err := os.Create(defaultPath)
	if err != nil {
		fmt.Println("Error creating file:", err)
		return
	}
	defer file.Close()

	_, err = file.Write(encodedConfig)
	if err != nil {
		fmt.Println("Error writing JSON to file:", err)
		return
	}

	fmt.Printf("\nConfiguration saved to: %s\n", defaultPath)
}

func parseInt(s string) int {
	i, err := strconv.Atoi(s)
	if err != nil {
		fmt.Println("Error parsing integer:", err)
		return 0
	}
	return i
}
