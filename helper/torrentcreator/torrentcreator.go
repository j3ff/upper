package torrentcreator

import (
	"fmt"
	"log"
	"os"
	"path/filepath"

	"upper/helper/mktorrent"

	"github.com/c2h5oh/datasize"
	"github.com/cheggaaa/pb"
	"github.com/pkg/errors"
)

func init() {
	log.SetFlags(0)
}

type profile struct {
	Name           string
	Announce       []string
	Private        bool
	MaxPieceLength string `toml:"max_piece_length"`
}

var profiles = []profile{
	{
		Name:           "green",
		Announce:       []string{"http://127.0.0.1:8080"},
		Private:        true,
		MaxPieceLength: "512 kb",
	},
}

type cf struct {
	Profiles []profile `toml:"profile"`
}

func (c cf) GetProfile(name string) *profile {
	for _, p := range c.Profiles {
		if p.Name == name {
			return &p
		}
	}
	return nil
}

func GetSize(path string) int64 {
	var size int64
	filepath.Walk(path, func(_ string, info os.FileInfo, err error) error {
		if err == nil && !info.IsDir() {
			size += info.Size()
		}
		return nil
	})
	return size
}

func CalculatePieceSize(size int64) string {
	switch {
	case size < 512*1024*1024:
		return fmt.Sprintf("%d kb", 256)
	case size <= 1*1024*1024*1024:
		return fmt.Sprintf("%d kb", 512)
	case size <= 3*1024*1024*1024:
		return fmt.Sprintf("%d mb", 1)
	case size <= 5*1024*1024*1024:
		return fmt.Sprintf("%d mb", 2)
	case size <= 10*1024*1024*1024:
		return fmt.Sprintf("%d mb", 4)
	case size <= 15*1024*1024*1024:
		return fmt.Sprintf("%d mb", 8)
	case size <= 20*1024*1024*1024:
		return fmt.Sprintf("%d mb", 16)
	default:
		return fmt.Sprintf("%d mb", 32)
	}
}

func UpdatePs(profiles []profile, path string) []profile {
	size := GetSize(path)
	maxPieceLength := CalculatePieceSize(size)
	for i := range profiles {
		profiles[i].MaxPieceLength = maxPieceLength
	}
	return profiles
}

func CreateTorrent(
	path string,
	customannURL string,
	saveDirectory string,
	goroutines int,
) error {
	UpdatePs(profiles, path)
	paths := []string{path}
	verbose := false
	profileName := "green"

	conf := cf{Profiles: profiles}
	pro := conf.GetProfile(profileName)

	if pro == nil {
		return errors.New("profile not found")
	}

	// announces := []string{customannURL}

	ps := mktorrent.Params{
		Announce: customannURL,
		Private:  pro.Private,
	}

	if pro.MaxPieceLength != "" {
		id := fmt.Sprintf("%s.max_piece_length", profileName)

		var bs datasize.ByteSize
		if err := bs.UnmarshalText([]byte(pro.MaxPieceLength)); err != nil {
			return errors.Wrapf(err, id)
		}
		if bs.Bytes() > uint64(mktorrent.AutoMaxPieceLength) {
			return fmt.Errorf("%s is too big", id)
		}
		ps.PieceLength = mktorrent.MaxPieceLength(int64(bs.Bytes()))

		if verbose {
			fmt.Printf("Max piece length: %s\n", pro.MaxPieceLength)
		}
	} else {
		ps.PieceLength = mktorrent.AutoPieceLength
	}

	if verbose {
		fmt.Printf("Profile: %s\n"+
			"Announce: %s\n"+
			"Private: %t\n",
			profileName,
			ps.Private)
	}

	for _, path := range paths {
		if err := func() error {
			parentDir := filepath.Dir(path)
			if err := os.Chdir(parentDir); err != nil {
				return errors.Wrap(err, "failed to change working directory")
			}

			ps.Path = filepath.Base(path)

			fs, err := mktorrent.NewFilesystem(ps)
			if err != nil {
				return errors.Wrap(err, "pre hashing")
			}

			pro := pb.New64(fs.PieceCount).Start()
			wt, err := fs.MakeTorrent(goroutines, pro)
			if err != nil {
				return errors.Wrap(err, "hashing")
			}
			pro.Finish()

			torrentFileName := filepath.Base(path) + ".torrent"
			torrentFilePath := filepath.Join(saveDirectory, torrentFileName)

			if derr := os.MkdirAll(saveDirectory, 0755); derr != nil {
				return errors.Wrap(derr, "failed to create save directory")
			}

			w, err := os.Create(torrentFilePath)
			fmt.Println("Torrent Saved in: ", torrentFilePath)
			if err != nil {
				return err
			}

			return errors.Wrap(wt.WriteTo(w), "can't save torrent")
		}(); err != nil {
			return errors.Wrapf(err, "%s", path)
		}
	}
	return nil
}
