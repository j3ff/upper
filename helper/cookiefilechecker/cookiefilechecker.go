package cookiefilechecker

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"upper/helper/cookiegenerator"
)

type CookieInfo struct {
	CookieName  string `json:"cookie_name"`
	CookieValue string `json:"cookie_value"`
}

func ContainsCookie(cookies []CookieInfo, name string) bool {
	for _, cookie := range cookies {
		if cookie.CookieName == name {
			return true
		}
	}
	return false
}

func Cookiescheckergen(username, password, cookieFilePath string) {
	if _, err := os.Stat(cookieFilePath); os.IsNotExist(err) {
		CookieSaver(username, password, cookieFilePath)
		return
	}

	cookiesJSON, err := os.ReadFile(cookieFilePath)
	if err != nil {
		log.Fatalf("Error reading cookie file: %v", err)
	}

	var cookies []CookieInfo
	if err := json.Unmarshal(cookiesJSON, &cookies); err != nil {
		log.Fatalf("Error parsing cookie file JSON: %v", err)
	}

	if !ContainsCookie(cookies, "access-token") {
		CookieSaver(username, password, cookieFilePath)
	}
}

func CookieSaver(username, password, cookieFilePath string) {
	cookiesJSON, err := cookiegenerator.GenerateCookies(username, password)
	if err != nil {
		log.Fatalf("Error generating cookies: %v", err)
	}

	if err := os.WriteFile(cookieFilePath, cookiesJSON, 0644); err != nil {
		log.Fatalf("Error saving cookies into file: %v", err)
	}

	fmt.Println("Cookies generated and saved successfully.")
}
