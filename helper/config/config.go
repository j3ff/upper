package config

import (
	"encoding/json"
	"os"
)

type EnvConfig struct {
	PtpImgApikey    string `json:"PTPImgAPI"`
	Username        string `json:"Username"`
	Password        string `json:"Password"`
	OmdbApikey    string `json:"OmdbApi"`
	TmdbApikey    string `json:"TmdbApi"`
	CookiePath      string `json:"CookiePath"`
	AnnounceUrl     string `json:"AnnounceUrl"`
	Workers         int    `json:"workers"`
	TorrentSavePath string `json:"TorrentSavePath"`
}

func LoadEnvConfig(filename string) (EnvConfig, error) {
	var config EnvConfig

	file, err := os.Open(filename)
	if err != nil {
		return config, err
	}
	defer file.Close()

	decoder := json.NewDecoder(file)
	err = decoder.Decode(&config)
	if err != nil {
		return config, err
	}

	return config, nil
}
