module upper

go 1.22.2

require (
	github.com/PuerkitoBio/goquery v1.9.1
	github.com/antchfx/htmlquery v1.3.1
	github.com/c2h5oh/datasize v0.0.0-20231215233829-aa82cc1e6500
	github.com/cheggaaa/pb v1.0.29
	github.com/cyruzin/golang-tmdb v1.6.1
	github.com/gorilla/websocket v1.5.1
	github.com/pkg/errors v0.9.1
	github.com/zeebo/bencode v1.0.0
	golang.org/x/net v0.24.0
	golang.org/x/term v0.19.0
)

require (
	github.com/andybalholm/cascadia v1.3.2 // indirect
	github.com/antchfx/xpath v1.3.0 // indirect
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/mattn/go-isatty v0.0.18 // indirect
	github.com/mattn/go-runewidth v0.0.15 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/rivo/uniseg v0.4.6 // indirect
	golang.org/x/sys v0.20.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)
