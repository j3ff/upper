package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"upper/helper/config"
	"upper/helper/configinteractor"
	"upper/helper/cookiefilechecker"
	"upper/helper/imguploader"
	"upper/helper/posterdl"
	"upper/helper/ptpimguploader"
	"upper/helper/torrentcreator"
	"upper/helper/usageinteractor"
)

func main() {
	var (
		videoPath      string
		ssfolderpath   string
		mode           string
		ct             bool
		helpFlag       bool
		envFile        string
		posterURL      string
		homeDir        string
		defaultEnvFile string
		version        = "0.0.2"
		showVer        bool
		makeConfig     bool
		usageinteract  bool
	)

	if runtime.GOOS == "windows" {
		homeDir, _ = os.UserHomeDir()
		defaultEnvFile = filepath.Join(homeDir, "Documents", "upper_env.json")
	} else {
		homeDir, _ = os.UserHomeDir()
		defaultEnvFile = filepath.Join(homeDir, ".config", "upper_env.json")
	}


	flag.StringVar(&videoPath, "f", "", "Path to the video file or folder")
	flag.StringVar(&ssfolderpath, "sf", "", "Path to the screenshot folder")
	flag.StringVar(&mode, "m", "i", `Mode: "i" for ipfs, "p" for ptpimg"`)
	flag.StringVar(&posterURL, "p", "", "URL of IMDB or TMDB for poster image")
	flag.StringVar(&envFile, "env", "", "Path to upper_env.json file")
	flag.BoolVar(&ct, "t", false, "Create torrent file")
	flag.BoolVar(&showVer, "v", false, "Show version")
	flag.BoolVar(&makeConfig, "c", false, "Create upper_env.json interactively")
	flag.BoolVar(&usageinteract, "inp", false, "Usage interactively")

	flag.BoolVar(&helpFlag, "h", false, "Show help")

	flag.Parse()

	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), "Usage: %s [options]\n", os.Args[0])
		fmt.Println("Options:")
		flag.PrintDefaults()
		if runtime.GOOS == "windows" {
			fmt.Printf(
				"\nDefault upper_env.json path: %s\n",
				filepath.Join(homeDir, "Documents", "upper_env.json"),
			)
		} else {
			fmt.Printf("\nDefault upper_env.json path: %s\n", filepath.Join(homeDir, ".config", "upper_env.json"))
		}
	}

	if helpFlag {
		flag.Usage()
		return
	}
	if makeConfig {
		configinteractor.ConfigInteractor()
		return
	}
	if showVer {
		fmt.Println("Version:", version)
		return
	}
	if usageinteract {
		usageinteractor.Run()
		return
	}
	if envFile == "" {
		if _, err := os.Stat(defaultEnvFile); os.IsNotExist(err) {
			fmt.Println("Default environment file not found:", defaultEnvFile)
			fmt.Println("Please specify the path to upper_env.json using the -env flag.")
			os.Exit(1)
		}
		envFile = defaultEnvFile
	}

	if ssfolderpath == "" && !ct {
		fmt.Println(
			"Please provide the path to the screenshot folder using the -sf flag or specify the -t flag for creating a torrent file.",
		)
		flag.Usage()
		return
	}

	if videoPath != "" && !ct && ssfolderpath == "" {
		fmt.Println("No task specified. Please specify a task using -sf or -t flags.")
		flag.Usage()
		os.Exit(1)
	}

	if ct && videoPath == "" {
		fmt.Println("Please provide file/folder path to create torrent using -f.")
		flag.Usage()
		os.Exit(1)
	}

	config, err := config.LoadEnvConfig(envFile)
	if err != nil {
		fmt.Println("Error loading config:", err)
		os.Exit(1)
	}

	if posterURL != "" {
		if ssfolderpath == "" {
			currentDir, cerr := os.Getwd()
			if cerr != nil {
				fmt.Println(cerr)
				os.Exit(1)
			}
			ssfolderpath = filepath.Join(currentDir, "upperss")
			cerr = os.MkdirAll(ssfolderpath, 0755)
			if cerr != nil {
				fmt.Println("Error creating upperss folder:", cerr)
				os.Exit(1)
			}
		}

		var poserr error
		poserr = posterdl.Dl(posterURL, ssfolderpath)
		if poserr != nil && (config.OmdbApikey != "" || config.TmdbApikey != "") {
			omdbAPI := config.OmdbApikey
			tmdbAPI := config.TmdbApikey
			poserr = posterdl.DlAPI(posterURL, omdbAPI, tmdbAPI, ssfolderpath)
		}

		if poserr != nil {
			fmt.Println("Error downloading poster:", poserr)
		}
	}

	switch mode {
	case "i":
		if ssfolderpath != "" {
			cookieFilePath := config.CookiePath

			if cookieFilePath == "" {
				fmt.Println("Error: Please specify cookie file path in upper_env.json")
				return
			}

			_, ckerr := os.Stat(cookieFilePath)
			if ckerr != nil {
				if os.IsNotExist(ckerr) {
					_, ferr := os.Create(cookieFilePath)
					if ferr != nil {
						fmt.Println("Error:", ferr)
						return
					}
				} else {
					fmt.Println("Error:", ckerr)
					return
				}
			}

			cksData, ckerr := os.ReadFile(cookieFilePath)
			if ckerr != nil {
				log.Fatal("Error reading cookie file:", ckerr)
			}

			var cks []struct {
				CkName  string `json:"cookie_name"`
				CkValue string `json:"cookie_value"`
			}
			ckerr = json.Unmarshal(cksData, &cks)
			if ckerr != nil || len(cks) == 0 || cks[0].CkValue == "" {
				ckerr = os.Remove(cookieFilePath)
				if ckerr != nil {
					log.Fatal("Error deleting cookie file:", ckerr)
				}

				if config.Username == "" || config.Password == "" {
					fmt.Println(
						"Error: Ensure to have both username and password in upper_env.json",
					)
					return
				}

				cookiefilechecker.Cookiescheckergen(
					config.Username,
					config.Password,
					cookieFilePath,
				)
			}

			url := "wss://iup.crowing.me/socket.io/?EIO=4&transport=websocket"
			folderPath := ssfolderpath
			cookiesPath := cookieFilePath
			uploader := imguploader.NewImageUploader(url, folderPath, cookiesPath)
			uploader.Run()
		} else {
			fmt.Println()
		}
	case "p":
		if ssfolderpath != "" {
			apiKey := config.PtpImgApikey
			urls, perr := ptpimguploader.UploadFiles(ssfolderpath, apiKey)
			if perr != nil {
				log.Fatal("Error:", perr)
			}
			for _, url := range urls {
				fmt.Println(url)
			}
		} else {
			fmt.Println()
		}
	default:
		fmt.Println("Unknown mode:", mode)
	}

	if ct && config.AnnounceUrl != "" && config.TorrentSavePath != "" {
		torrentSavePath := config.TorrentSavePath
		announceURL := config.AnnounceUrl
		workers := config.Workers
		torerr := torrentcreator.CreateTorrent(videoPath, announceURL, torrentSavePath, workers)
		if torerr != nil {
			log.Fatal("Error creating torrent file:", torerr)
		}
	} else if ct {
		log.Fatal("Please provide both AnnounceURL and TorrentSavePath in the configuration")
	}
}
