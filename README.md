# upper

* Standalone, No need of any external dependencies.

**Download latest** :- [Here](https://codeberg.org/j3ff/upper/releases)

**Usage:**

```
upper [flags] -f <video_file> [-sf <screenshot_folder>] [-p <imdb or tmdb url>] [-m <mode>] [-env <env_file>]
```

**Flags:**

* `-f string`: Path to the video file or folder
* `-sf string`: Path to the screenshot folder
* `-p string`: URL of the IMDB or TMDB (optional)
* `-m string`: Mode of operation (default "i" for IPFS, "p" for PTPImg) (optional)
* `-env string`: Path to the upper_env.json file (default: ~/.config/upper_env.json on Linux/macOS, %USERPROFILE%\Documents\upper_env.json on Windows)
* `-c`: Create upper_env.json in default path interactively
* `-inp`: Usage of upper in interactive mode
* `-t`: Create torrent file (optional)
* `-v`: Show version number
* `-h`: Display help message

**Configuration:**

The script expects a configuration file named `upper_env.json` either in the default location (see above) or custom path via `-env` flag. The file should contain the following settings:

* `Username`: Username for IPFS mode
* `Password`: Password for IPFS mode
* `OmdbApi`: OMDB Api key (for IMDB poster [optional])
* `TmdbApi`: TMDB Api key (for TMDB poster [optiona])
* `CookiePath`: Path to the cookie file for IPFS mode
* `AnnounceUrl`: Announce url for torrent creation (see upload page on site)
* `TorrentSavePath`: Path to save the torrent file
* `Workers`: Number of Threads for torrent creation
* `PtpImgApikey`: API key for PTPImg mode

**Sample upper_env.json**:- [Here](https://codeberg.org/j3ff/upper/src/branch/main/upper_env_sample.json)

# A few Examples:
**General:**
* `upper -c` - Interactively prepare upper_env.json
* `upper -inp` - Interactively use program without need of other cli flags

**Linux/Mac:**
* `upper -f video.mp4 -sf /path/to/screenshots -p htts://www.imdb.com/tt12345678 -m i`
* `upper -f /path/to/dvd_folder -sf /path/to/screenshots -p htts://www.imdb.com/tt12345678 -m i -t`
* `upper -f video.mp4 -sf /path/to/screenshots -m p -env /path/to/custom_env.json`

**Windows:**
* `upper -f D:\\path\\to\\video.mp4 -sf D:\\path\\to\\screenshots -p https://www.imdb.com/tt12345678 -m i`
* `upper -f D:\\path\\to\\dvd_folder -sf D:\\path\\to\\screenshots -p https://www.imdb.com/tt12345678 -m i -t`
* `upper -f D:\\path\\to\\video.mp4 -sf D:\\path\\to\\screenshots -m p -env D:\\path\\to\\custom_env.json`

# Compile:
- Git clone the repository locally, ensuring Golang is installed.
- Run `go build`.

*Note:* Prebuilt executables are available in the Releases section.
